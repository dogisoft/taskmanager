function post_ajax_data(url, encodedata, success) {
    $.ajax({
        type: "POST",
        url: url,
        data: encodedata,
        dataType: "json",
        restful: true,
        contentType: 'application/json',
        cache: false,
        timeout: 20000,
        async: true,
        beforeSend: function (data) {
        },
        success: function (data) {
            success.call(this, data);
        },
        error: function (data) {
            alert("Status text: "+data.statusText);
        }
    });
}

function ajax_data(type, url, success) {
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        restful: true,
        cache: false,
        timeout: 20000,
        async: true,
        beforeSend: function (data) {
        },
        success: function (data) {
            success.call(this, data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}


function fillSelectForm(){

    var base_url = "http://localhost:8080/sintesis/";
    /* Load category */
    url = base_url + 'api/categories';

    ajax_data('GET', url, function (data) {
        var form = "<select id=\"parentID\"><option value='0'>-- TOP LEVEL --</option>";
        var leng = Object.keys(data.categories).length;

        for (var i = 0; i < leng; i++) {
            form += "<option value='" + data.categories[i].categoryID + "'>" + data.categories[i].categoryID + ". " + data.categories[i].name + "</option>";
        }
        form += "</select>";
        $(form).appendTo("#mainContent");
    });
}