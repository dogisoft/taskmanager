<?php
include 'db.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/todos','getTodos');
$app->post('/inserttodo', 'insertTodo');
$app->post('/newcategory', 'newCategory');
$app->get('/categories','listCategories');
$app->get('/catlist','catList');
$app->get('/tasklist','taskList');
$app->get('/category/edit/:categoryID','getCategory');
$app->get('/deletecategory/delete/:categoryID','deleteCategory');
$app->get('/deletetask/delete/:taskid','deleteTask');
//$app->delete('/updates/delete/:update_id','deleteUpdate');
//$app->get('/users/search/:query','getUserSearch');

$app->run();
/*List of category*/
function catList() {
	$sql = "SELECT categoryID, parentID, name FROM task_category ORDER BY categoryID";
	try {
		$db = getDB();
		$stmt = $db->query($sql);

		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo  json_encode($tasks);
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

/*delete by id*/
function deleteCategory($categoryID) {

	$sql = "DELETE FROM task_category WHERE categoryID=:categoryID";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("categoryID", $categoryID);
		$stmt->execute();
		$db = null;
		echo true;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}

/*Get Item data by id*/
function getCategory($categoryID) {

	$sql = "SELECT categoryID, parentID, name FROM task_category WHERE categoryID=:categoryID";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("categoryID", $categoryID);
		$stmt->execute();
		$updates = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($updates);

	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/*List of tasks*/
function taskList() {
	$sql = "SELECT taskid, catid, taskid, name FROM task ORDER BY taskid";
	try {
		$db = getDB();
		$stmt = $db->query($sql);
		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($tasks);
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

/*Get list of To dos*/
function getTodos() {
	$sql = "SELECT taskid, name FROM task ORDER BY taskid";
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"tasks": ' . json_encode($tasks) . '}';
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

/*List categories*/
function listCategories() {
	$sql = "SELECT categoryID, parentID, name FROM task_category ORDER BY name";
	try {
		$db = getDB();
		$stmt = $db->query($sql);
		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"categories": ' . json_encode($tasks) . '}';
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function newCategory() {
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());
	$sql = "INSERT INTO task_category (parentID, name) VALUES (:parentID, :name)";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("parentID", $update->parentID);
		$stmt->bindParam("name", $update->name);

		$stmt->execute();
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function insertTodo() {
	$request = \Slim\Slim::getInstance()->request();
	$update = json_decode($request->getBody());
	$sql = "INSERT INTO task (name, todo_from, todo_to, description, catid) VALUES (:name, :todo_from, :todo_to, :description, :catid)";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("name", $update->name);
		$stmt->bindParam("todo_from", $update->todo_from);
		$stmt->bindParam("todo_to", $update->todo_to);
		$stmt->bindParam("description", $update->description);
		$stmt->bindParam("catid", $update->catid);
		$stmt->execute();
	} catch(PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getUserUpdates() {
	$sql = "SELECT A.user_id, A.username, A.name, A.profile_pic, B.update_id, B.user_update, B.created FROM users A, updates B WHERE A.user_id=B.user_id_fk  ORDER BY B.update_id DESC";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql); 
		$stmt->execute();		
		$updates = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"updates": ' . json_encode($updates) . '}';
		
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getUserUpdate($update_id) {
	$sql = "SELECT A.user_id, A.username, A.name, A.profile_pic, B.update_id, B.user_update, B.created FROM users A, updates B WHERE A.user_id=B.user_id_fk AND B.update_id=:update_id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
        $stmt->bindParam("update_id", $update_id);		
		$stmt->execute();		
		$updates = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"updates": ' . json_encode($updates) . '}';
		
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function deleteTask($taskid) {
   
	$sql = "DELETE FROM task WHERE taskid=:taskid";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("taskid", $taskid);
		$stmt->execute();
		$db = null;
		echo true;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
	
}

function getUserSearch($query) {
	$sql = "SELECT user_id,username,name,profile_pic FROM users WHERE UPPER(name) LIKE :query ORDER BY user_id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$query = "%".$query."%";  
		$stmt->bindParam("query", $query);
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"users": ' . json_encode($users) . '}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}
?>