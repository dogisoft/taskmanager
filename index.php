<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Create RESTful API using Slim PHP Framework</title>
    <link href='css/style.css' rel='stylesheet' type='text/css'/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/angular.min.js"></script>

    <script src="js/ajaxGetPost.js"></script>
</head>
<body>

<div style="margin:0 auto;width:1000px;">
    <h1>ToDo Administrator v.01</h1>

    <div>
        <a href="?page=newtask"> New task </a> | <a href="?page=tasklist"> Task List </a> | <a href="?page=newcategory">
            New Category </a> | <a href="?page=catlist"> Category List </a>
    </div>
    <br>
    <hr>
    <br>
    <?php
    if (isset($_GET['page'])) {
        require("pages/" . $_GET['page'] . ".php");
    } else {
        require("pages/home.php");
    }
    ?>
</div>
</body>
</html>