<script>
    /* Fill the select item */
    $(document).ready(function () {
        fillSelectForm();
    });

    /* Insert new task */
    $('body').on("click", '.stpostbutton', function () {

        encode = JSON.stringify({
            "name": $('#name').val(),
            "catid": $('#parentID').val(),
            "todo_from": $('#todo_from').val(),
            "todo_to": $('#todo_to').val(),
            "description": $('#description').val()
        });
        var base_url = "http://localhost:8080/sintesis/";
        var url = base_url + 'api/inserttodo';
        post_ajax_data(url, encode, function (data) {
            console.log(data);
        });

    });
</script>

<h2>Create Task</h2>

<div id="form">
    <div><input type="text" id="name" placeholder="Task title"/></div>

    <div id="mainContent"></div>
    <div><input type="text" id="todo_from" placeholder="From"/></div>
    <div><input type="text" id="todo_to" placeholder="To"/></div>
    <div><textarea id="description" placeholder="Description"></textarea></div>

    <div><input type="submit" value="Create Task" class="stpostbutton"></div>
</div>
