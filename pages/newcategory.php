<script>
    $(document).ready(function () {
        fillSelectForm();
    });


    var base_url = "http://localhost:8080/sintesis/";

    /* Insert new category */
    $('body').on("click", '.createcategory', function () {

        encode = JSON.stringify({
            "name": $('#categoryName').val(),
            "parentID": $('#parentID').val()
        });
        url = base_url + 'api/newcategory';
        post_ajax_data(url, encode, function (data) {

        });

    });
</script>

<h2>Create Category</h2>
<div id="categoryForm">
    <div><input type="text" id="categoryName" placeholder="Title of category"/></div>
    <label>Parent category</label>
    <div id="mainContent"></div>
    <div><input type="submit" value="Create new category" class="createcategory"></div>
</div>