<script>
    angular.module('ngAppSintesis', []).controller('ngAppTasklistController', function ($scope, $http) {

        var base_url = "http://localhost:8080/sintesis/";
        url = base_url + 'api/tasklist';

        $scope.renderPage = function(){
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                $scope.result = response;
                // this callback will be called asynchronously
                // when the response is available
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        };

        //Delete item
        $scope.delete = function ( idx ) {
            url = base_url + 'api/deletetask/delete/'+idx;
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                location.reload();

            }, function errorCallback(response) {

            });
        };

        $scope.renderPage();
    });

</script>

<div ng-app="ngAppSintesis">

    <h2>List of category</h2>

    <div id="catlist" ng-controller="ngAppTasklistController">
        <div class="dataTable" ng-repeat="item in result.data">
            <div class="rowID">{{item.taskid}}</div>
            <div class="rowName">{{item.name}}</div>
            <div class="rowID">{{item.catid}}</div>
            <div class="rowDelete"><button ng-click="delete(item.taskid)">Delete</button></div>
        </div>
    </div>
</div>